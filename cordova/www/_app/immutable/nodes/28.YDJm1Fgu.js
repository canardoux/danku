import{s as ne,a as D,l as oe,f as s,g as P,i as x,e as l,t as b,c as r,b as w,m as y,d as L,o as I,h as e,n as ie}from"../chunks/scheduler.YI9nFNpH.js";import{S as ae,i as le,c as K,a as Q,m as X,t as Z,b as ee,d as te}from"../chunks/index.UPYdoUQi.js";import"../chunks/platform.-6nnqGW1.js";import{P as re}from"../chunks/PageEmpty.3hDyQWuD.js";import{S as se}from"../chunks/SourceButton.ME8Y_qWy.js";function ce(u){let o,t,i,n="<ion-menu-button></ion-menu-button>",c,h,m,R,v,q="Page",C,f,_,E,j="<h1>API specs for IonPage</h1>",k,d,S,A,H,V,F=`  ionViewWillEnter
  ionViewDidEnter
  ionViewWillLeave
  ionViewDidLeave
`,B,g,M,z,U,Y,N;return m=new se({props:{name:"Page"}}),{c(){o=l("ion-header"),t=l("ion-toolbar"),i=l("ion-buttons"),i.innerHTML=n,c=D(),h=l("ion-buttons"),K(m.$$.fragment),R=D(),v=l("ion-title"),v.textContent=q,C=D(),f=l("ion-content"),_=l("ion-card"),E=l("ion-card-header"),E.innerHTML=j,k=D(),d=l("ion-card-content"),S=b(`This intends to show the working of the IonPage component. This components the lifecycle
				hooks and proper animation for a page shown in your app. The following lifecycle hooks are
				provided for:
				`),A=l("br"),H=D(),V=l("pre"),V.textContent=F,B=b(`
				You can bind the hook using simple function binding.
				`),g=l("pre"),M=b("  "),z=b(u[4]),U=b(`
`),Y=b(`

				Easy does it!`),this.h()},l(a){o=r(a,"ION-HEADER",{translucent:!0});var $=w(o);t=r($,"ION-TOOLBAR",{});var T=w(t);i=r(T,"ION-BUTTONS",{slot:!0,"data-svelte-h":!0}),y(i)!=="svelte-u1d3uo"&&(i.innerHTML=n),c=P(T),h=r(T,"ION-BUTTONS",{slot:!0});var G=w(h);Q(m.$$.fragment,G),G.forEach(s),R=P(T),v=r(T,"ION-TITLE",{"data-svelte-h":!0}),y(v)!=="svelte-f989dd"&&(v.textContent=q),T.forEach(s),$.forEach(s),C=P(a),f=r(a,"ION-CONTENT",{fullscreen:!0,class:!0});var J=w(f);_=r(J,"ION-CARD",{});var O=w(_);E=r(O,"ION-CARD-HEADER",{"data-svelte-h":!0}),y(E)!=="svelte-o2xuez"&&(E.innerHTML=j),k=P(O),d=r(O,"ION-CARD-CONTENT",{});var p=w(d);S=L(p,`This intends to show the working of the IonPage component. This components the lifecycle
				hooks and proper animation for a page shown in your app. The following lifecycle hooks are
				provided for:
				`),A=r(p,"BR",{}),H=P(p),V=r(p,"PRE",{"data-svelte-h":!0}),y(V)!=="svelte-3by5mm"&&(V.textContent=F),B=L(p,`
				You can bind the hook using simple function binding.
				`),g=r(p,"PRE",{});var W=w(g);M=L(W,"  "),z=L(W,u[4]),U=L(W,`
`),W.forEach(s),Y=L(p,`

				Easy does it!`),p.forEach(s),O.forEach(s),J.forEach(s),this.h()},h(){I(i,"slot","start"),I(h,"slot","end"),I(o,"translucent",!0),I(f,"fullscreen",""),I(f,"class","ion-padding")},m(a,$){x(a,o,$),e(o,t),e(t,i),e(t,c),e(t,h),X(m,h,null),e(t,R),e(t,v),x(a,C,$),x(a,f,$),e(f,_),e(_,E),e(_,k),e(_,d),e(d,S),e(d,A),e(d,H),e(d,V),e(d,B),e(d,g),e(g,M),e(g,z),e(g,U),e(d,Y),N=!0},p:ie,i(a){N||(Z(m.$$.fragment,a),N=!0)},o(a){ee(m.$$.fragment,a),N=!1},d(a){a&&(s(o),s(C),s(f)),te(m)}}}function de(u){let o,t,i;return t=new re({props:{ionViewWillEnter:u[0],ionViewDidEnter:u[1],ionViewWillLeave:u[2],ionViewDidLeave:u[3],$$slots:{default:[ce]},$$scope:{ctx:u}}}),{c(){o=D(),K(t.$$.fragment),this.h()},l(n){oe("svelte-17lp6ze",document.head).forEach(s),o=P(n),Q(t.$$.fragment,n),this.h()},h(){document.title="Ionic Companion - Page"},m(n,c){x(n,o,c),X(t,n,c),i=!0},p(n,[c]){const h={};c&32&&(h.$$scope={dirty:c,ctx:n}),t.$set(h)},i(n){i||(Z(t.$$.fragment,n),i=!0)},o(n){ee(t.$$.fragment,n),i=!1},d(n){n&&s(o),te(t,n)}}}function ue(u){return[()=>{console.log("Page:ionViewWillEnter")},()=>{console.log("Page:ionViewDidEnter")},()=>{console.log("Page:ionViewWillLeave")},()=>{console.log("Page:ionViewDidLeave")},`...
  const ionViewWillEnter=
        ionViewDidEnter=
        ionViewWillLeave=
        ionViewDidLeave=console.log('We got an event!') 
  
  <IonPage {ionViewWillEnter} {ionViewDidEnter} {ionViewWillLeave} {ionViewDidLeave}>
  
  `]}class ge extends ae{constructor(o){super(),le(this,o,ue,de,ne,{})}}export{ge as component};

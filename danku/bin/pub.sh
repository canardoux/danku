#!/bin/bash

#source ../bin/new-version.sh
. DANKU_VERSION

echo "******************* v$DANKU_VERSION *****************"

#source ../bin/cordova-pub.sh

cd build
tar czf /tmp/danku.tgz --no-xattrs *
cd ..

scp /tmp/danku.tgz danku@danku:/tmp
ssh   danku@danku "rm -rf /var/www/canardoux.xyz/danku/live/*  ; tar xzf /tmp/danku.tgz -C /var/www/canardoux.xyz/danku/live;  rm /tmp/danku.tgz"

#cp -r .svelte-kit/output/prerendered/pages/* .svelte-kit/output/client
#cp -r .svelte-kit/output/prerendered/pages/* .svelte-kit/output/server

#cp -r .svelte-kit/output/prerendered/pages/* .svelte-kit/output/client
#cp -r .svelte-kit/output/prerendered/pages/* .svelte-kit/output/server


cd ..
git add .
git commit -m "Danku-svelte : Version $DANKU_VERSION"
git pull origin
git push origin
#if [ ! -z "$DANKU_VERSION" ]; then
    git tag -f $DANKU_VERSION
    git push  -f origin $DANKU_VERSION
#fi


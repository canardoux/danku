#!/bin/bash

ssh danku@danku "rm -rf /var/www/canardoux.xyz/danku/downloads/*"
ssh danku@danku  "touch /var/www/canardoux.xyz/danku/downloads/v$DANKU_VERSION"
pwd
scp ../cordova/platforms/electron/build/danku-$DANKU_VERSION-universal.dmg danku@danku:/var/www/canardoux.xyz/danku/downloads/danku-universal.dmg
scp ../cordova/platforms/electron/build/danku\ Setup\ *.exe danku@danku:/var/www/canardoux.xyz/danku/downloads/Danku-Setup.exe
scp ../cordova/platforms/electron/build/xyz.canardoux.danku-$DANKU_VERSION.tar.gz danku@danku:/var/www/canardoux.xyz/danku/downloads/xyz.canardoux.danku-amd64.tar.gz
scp ../cordova/platforms/electron/build/xyz.canardoux.danku-$DANKU_VERSION-arm64.tar.gz danku@danku:/var/www/canardoux.xyz/danku/downloads/xyz.canardoux.danku-arm64.tar.gz
scp ../cordova/platforms/electron/build/xyz.canardoux.danku_"$DANKU_VERSION"_amd64.deb danku@danku:/var/www/canardoux.xyz/danku/downloads/xyz.canardoux.danku-amd64.deb
scp ../cordova/platforms/electron/build/xyz.canardoux.danku_"$DANKU_VERSION"_arm64.deb danku@danku:/var/www/canardoux.xyz/danku/downloads/xyz.canardoux.danku-arm64.deb
scp ../cordova/platforms/android/app/build/outputs/apk/release/app-release.apk danku@danku:/var/www/canardoux.xyz/danku/downloads/danku.apk

#!/bin/bash


export PYTHON_PATH=/Library/Frameworks/Python.framework/Versions/2.7/bin/python

echo "************************ v$DANKU_VERSION ********************"
cd ../cordova


gsed -i  "s/^\( *\"version\": *\"\).*$/\1$DANKU_VERSION\",/"     package.json
gsed -i  "s/^\( *<widget id=\"xyz.canardoux.danku\" version=\"\)[^\"]*\(.*\)$/\1$DANKU_VERSION\2/"     config.xml

cordova clean
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
fi

rm -rf platforms/*

cordova prepare $1 
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
fi

cordova build $1 --release
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
fi

cd /tmp


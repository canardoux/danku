#!/bin/bash


. DANKU_VERSION
major="${DANKU_VERSION%.*}" 
minor="${DANKU_VERSION#*.}"  # remove everything until the first '.'
pat="${minor#*.}" # remove everything until the second '.'
let "++pat"
DANKU_VERSION=$major.$pat
echo DANKU_VERSION=$DANKU_VERSION
echo DANKU_VERSION=$DANKU_VERSION > DANKU_VERSION
. DANKU_VERSION


echo "******************* v$DANKU_VERSION *******************"